#Инструкция по запуску проекта

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: 

Terminal => Run Task... => навести мышь на один из вариантов выбора => ...

... => правее должна появиться синяя шестеренка "configured tasks" => нажмите на шестеренку => откроется файл tasks.json

Для сборки проекта используется команда maven build

Для сборки и последующего запуска проекта используется команда maven build and run

Код:

{
    "tasks": [
        {
            "type": "che",
            "label": "maven build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/maven-projects-part6",
                "component": "maven"
            }
        },
        {
            "type": "che",
            "label": "maven build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/maven-projects-part6",
                "component": "maven"
            }
        },
         {
            "type": "che",
            "label": "run webapp",
            "command": "java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8080 \\\ntarget/*.jar\n",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/maven-projects-part6",
                "component": "tools"
            }
        }
    ]
}